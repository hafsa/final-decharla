# Generated by Django 4.1.7 on 2023-06-12 11:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('DeCharla', '0008_remove_sala_megusta_voto_megusta'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mensaje',
            name='imagen',
            field=models.ImageField(blank=True, null=True, upload_to='imagenes'),
        ),
    ]
