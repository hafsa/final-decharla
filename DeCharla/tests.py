import json
from datetime import datetime
from django.test import Client
from django.test import TestCase
from django.urls import reverse
from DeCharla.models import Sesion, Sala, Voto, Contrasena, Mensaje
from DeCharla.views import pie_pagina, set_language

# Create your tests here.
# path('', views.index, name='index'), HECHO
# path('configuracion', views.manejar_configuracion, name='configuracion'), HECHO
# path('login', views.manejar_login, name='login'), HECHO
# path('logout', views.manejar_logout, name='logout'), HECHO
# path('help', views.help, name='help'), HECHO
# path('<str:sala>', views.manejar_salas, name='manejar_salas'), HECHO
# path('votar/', views.manejar_votos, name='manejar_votos'),
# path('desvotar/', views.manejar_votos, name='manejar_votos'),
# path('crear_sala/', views.crear_sala, name='crear_sala'),
# path('sala_json/<str:sala>', views.sala_json, name='sala_json'), HECHO
# path('sala_dinamica/<str:sala>', views.sala_dinamica, name='sala_dinamica') HECHO

#TEST EXTREMO A EXTREMO POR CADA RECURSO

#TEST RECURSO LOGIN
class Login(TestCase):
    def test_login_valido(self):
        """Test con contraseña válida para el recurso de login"""
        #Creamos el cliente y el objeto contraseña válida
        client = Client()
        Contrasena.objects.create(contrasena='Holaquepasa')

        #Solicitamos página de login mediante un GET y comprobamos que nos devuelve un 200 OK
        response = client.get('/DeCharla/login')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/login.html') #Aquí comprobamos página de login

        #Rellenamos el formulario y hacemos POST y comprobamos que nos redirecciona correctamente
        response = client.post('/DeCharla/login', {'contrasena': 'Holaquepasa'}) #el cliente rellena el formulario de login
        self.assertEqual(response.status_code, 301) #Se hace la redirección correctamente
        self.assertEqual(response.url, '/DeCharla/') #comprobamos si nos ha redireccionado a esta url que es la de index

        #Comprobamos que el servidor nos ha devuelto una set-cookie llamada cookie_user al realizar el POST
        cookie_value = response.cookies.get('cookie_user')
        self.assertIsNotNone(cookie_value) #comprobamos que el valor de la cookie no es vacío

    def test_login_novalido(self):
        """Test con contraseña invalida para el recurso de login"""

        client = Client()
        Contrasena.objects.create(contrasena='Holaquepasa')

        response = client.get('/DeCharla/login')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/login.html')

        response = client.post('/DeCharla/login', {'contrasena': 'si'})
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.content.decode(), 'Unauthorized')

        self.assertNotIn('cookie_user', response.cookies)

#TEST UNITARIO DE LOGIN Y LA COOKIE
#Para no comprobarlo en todas las funciones, he dedicido crear un test especifico para ello
class CookieTestCase(TestCase):
    def test_cookie(self):
        """Con este test comprobaremos que si el cliente no incluye la cookie en sus peticiones,
        el servidor le redirecciona a login directamente """
        client = Client()

        #PÁGINA PRINCIPAL
        response = client.get('/DeCharla/')
        self.assertEqual(response.status_code, 302)

        #PÁGINA DE CONFIGURACION
        response = client.get('/DeCharla/configuracion')
        self.assertEqual(response.status_code, 302)

        #PÁGINA DE HELP
        response = client.get('/DeCharla/help')
        self.assertEqual(response.status_code, 302)


#TEST RECURSO HELP
class HelpTestCase(TestCase):
    def test_help(self):
        """Test para el recurso help"""
        client = Client()
        Contrasena.objects.create(contrasena='Holaquepasa')

        response = client.get('/DeCharla/login')
        self.assertEqual(response.status_code, 200)

        response = client.post('/DeCharla/login', {'contrasena': 'Holaquepasa'})
        self.assertEqual(response.status_code, 301)

        response = client.get('/DeCharla/help')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'DeCharla/help.html')


#TEST RECURSO LOGOUT
class LogoutTestCase(TestCase):
    def test_logout(self):
        """Test para el recurso de logout"""
        client = Client()
        client.cookies['cookie_user'] = 'test_cookie_user'
        response = client.get(reverse('logout'))

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('login'))

#RECURSO INDEX
class IndexTestCase(TestCase):
    def test_index_concookie(self):
        """Test para el recurso Index suponiendo que ya existe la cookie"""
        client = Client()
        Contrasena.objects.create(contrasena='Holaquepasa')
        response = client.post('/DeCharla/login', {'contrasena': 'Holaquepasa'})

        cookie_value = response.cookies.get('cookie_user')
        client.cookies['cookie_user'] = cookie_value

        response = client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'DeCharla/index.html')
        content = response.content.decode('utf-8')
        self.assertIn('<title>DeCharla</title>', content)

#TEST RECURSO CONFIGURACIÓN
class ConfiguracionTestCase(TestCase):
    def test_configuracion(self):
        """Test para el recurso Configuración"""
        client = Client()
        Contrasena.objects.create(contrasena='Holaquepasa')
        response = client.post('/DeCharla/login', {'contrasena': 'Holaquepasa'})

        cookie_value = response.cookies.get('cookie_user')
        client.cookies['cookie_user'] = cookie_value

        response = client.get(reverse('configuracion'))
        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(response, 'DeCharla/configuracion.html')
        content = response.content.decode('utf-8')
        self.assertIn('<form class="mt-4" method="post">', content)

        # Realizar la solicitud POST al recurso configuración
        form_data = {
            'nombre': 'Nuevo nombre',
            'tamano_fuente': '14',
            'tipo_fuente': 'Verdana'
        }

        response = client.post(reverse('configuracion'), data=form_data)
        self.assertEqual(response.status_code, 200)

#TEST RECURSO SALA
class SalaTestCase(TestCase):
    def test_sala(self):
        """Test para el recurso sala: cuando realizamos un GET a  la página de una sala"""
        client = Client()
        Contrasena.objects.create(contrasena='Holaquepasa')
        response = client.post('/DeCharla/login', {'contrasena': 'Holaquepasa'})

        cookie_value = response.cookies.get('cookie_user')
        client.cookies['cookie_user'] = cookie_value
        sesion = Sesion.objects.create(cookie_user=cookie_value)
        sala = Sala.objects.create(creador=sesion, nombre='Sala de prueba', fecha=datetime.now())

        #REALIZAMOS UN GET A LA SALA DE PRUEBA QUE HEMOS CREADO
        response = client.get(reverse('manejar_salas', args=[sala.nombre]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'DeCharla/sala.html')

        #ESCRIBIMOS UN MENSAJE EN EL FORMULARIO DE AÑADIR MENSAJES
        form_data = {
            'contenido': 'Nuevo mensaje de prueba',
        }
        response = client.post(reverse('manejar_salas', args=[sala.nombre]), data=form_data)
        self.assertEqual(response.status_code, 200)

        #COMPROBAMOS QUE LA ACTUALIZACIÓN DE MENSAJE MEDIANTE PUT FUNCIONA
        put_data = {
            'contenido': 'Mensaje funciona',
        }
        response = client.put(reverse('manejar_salas', args=[sala.nombre]), data=put_data)
        self.assertEqual(response.status_code, 200)

#TEST PARA LOS RECURSOS VOTAR Y DESVOTAR
class VotarTestCase(TestCase):
    def test_votar(self):
        client = Client()
        Contrasena.objects.create(contrasena='Holaquepasa')
        response = client.post('/DeCharla/login', {'contrasena': 'Holaquepasa'})

        cookie_value = response.cookies.get('cookie_user')
        client.cookies['cookie_user'] = cookie_value

        # nos creamos una sala de prueba
        form_data = {
            'nombre_sala': 'Sala de prueba',
        }
        response = client.post('/DeCharla/crear_sala/', data=form_data)
        self.assertEqual(response.status_code, 302)

        sesion = Sesion.objects.create(cookie_user=cookie_value)
        data = {
            'action': 'votar/',
            'sala': 'Sala de prueba'
        }
        response = client.post('/DeCharla/votar/', data)
        self.assertEqual(response.status_code, 302)
        sala = Sala.objects.create(creador=sesion, nombre='Sala de Prueba', fecha=datetime.now())
        voto = Voto.objects.create(sala=sala, sesion=sesion, megusta=True)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(voto.megusta)
        self.assertRedirects(response, reverse('index'))

    def test_desvotar(self):
        client = Client()
        Contrasena.objects.create(contrasena='Holaquepasa')
        response = client.post('/DeCharla/login', {'contrasena': 'Holaquepasa'})

        cookie_value = response.cookies.get('cookie_user')
        client.cookies['cookie_user'] = cookie_value

        # nos creamos una sala de prueba
        form_data = {
            'nombre_sala': 'Sala de prueba',
        }
        response = client.post('/DeCharla/crear_sala/', data=form_data)
        self.assertEqual(response.status_code, 302)

        sesion = Sesion.objects.create(cookie_user=cookie_value)
        data = {
            'action': 'desvotar/',
            'sala': 'Sala de prueba'
        }
        response = client.post('/DeCharla/desvotar/', data)
        self.assertEqual(response.status_code, 302)
        sala = Sala.objects.create(creador=sesion, nombre='Sala de Prueba', fecha=datetime.now())
        voto = Voto.objects.create(sala=sala, sesion=sesion, megusta=False)
        self.assertEqual(response.status_code, 302)
        self.assertFalse(voto.megusta)
        self.assertRedirects(response, reverse('index'))

#TEST PARA EL RECURSO CREAR SALA
class CrearSalaTestCase(TestCase):
    def test_crear_sala(self):
        """Test para el recurso Crear Sala"""
        client = Client()
        Contrasena.objects.create(contrasena='Holaquepasa')
        response = client.post('/DeCharla/login', {'contrasena': 'Holaquepasa'})

        cookie_value = response.cookies.get('cookie_user')
        client.cookies['cookie_user'] = cookie_value
        form_data = {
            'nombre_sala': 'Sala de prueba',
        }
        response = client.post('/DeCharla/crear_sala/', data=form_data)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('manejar_salas', args=['Sala de prueba']))


#TEST PARA EL RECURSO SALA DINÁMICA
class SalaDinamicaTestCase(TestCase):
    def test_sala_dinamica(self):
        """Tst para el recurso de sala dinámica"""
        client = Client()
        Contrasena.objects.create(contrasena='Holaquepasa')
        response = client.post('/DeCharla/login', {'contrasena': 'Holaquepasa'})

        cookie_value = response.cookies.get('cookie_user')
        client.cookies['cookie_user'] = cookie_value

        form_data = {
            'nombre_sala': 'Sala de prueba',
        }
        response = client.post('/DeCharla/crear_sala/', data=form_data)
        self.assertEqual(response.status_code, 302)

        response = client.get('/DeCharla/sala_dinamica/Sala de prueba')
        self.assertEqual(response.status_code, 200)

        content = response.content.decode('utf-8')

#TEST PARA EL RECURSO JSON DE CADA SALA
class JsonTestCase(TestCase):
    def test_json(self):
        """Test para el recurso JSON de cada sala"""
        client = Client()
        Contrasena.objects.create(contrasena='Holaquepasa')
        response = client.post('/DeCharla/login', {'contrasena': 'Holaquepasa'})

        cookie_value = response.cookies.get('cookie_user')
        client.cookies['cookie_user'] = cookie_value

        # CREAMOS UNA SALA DE PRUEBA
        form_data = {
            'nombre_sala': 'Sala de prueba',
        }
        response = client.post('/DeCharla/crear_sala/', data=form_data)
        self.assertEqual(response.status_code, 302)

        #AÑADIMOS UN MENSAJE DE PRUEBA PARA PODER COMPROBAR EL JSON
        form_data = {
            'contenido': 'Nuevo mensaje de prueba',
        }
        response = client.post(reverse('manejar_salas', args=['Sala de prueba']), data=form_data)
        self.assertEqual(response.status_code, 200)

        response = client.get('/DeCharla/sala_json/Sala de prueba')
        self.assertEqual(response.status_code, 200)

        json_sala = json.loads(response.content)
        mensaje = json_sala[0]
        self.assertIn('author', mensaje) #Nos tiene quedvolver Anónimo
        self.assertIn('text', mensaje) #Nuevo mensaje de prueba

#TEST RECURSO SET LENGUAGE
class SetLenguageTestCase(TestCase):
    def test_set_language(self):
        "Test recurso de set_lenguage"
        client = Client()
        response = client.post('/set_language/', {'language': 'Español'})
        self.assertEqual(response.status_code, 302)

#INCLUSIÓN DE ALGÚN TEST UNITARIO
class UnitariosTestCase(TestCase):
    def test_pie_pagina(self):
        "Test unitario de la función pie_página"

        #Simulamos una base de datos
        sesion = Sesion.objects.create(cookie_user='cookie_user2345')
        sesion.save()
        sala = Sala.objects.create(creador=sesion, nombre='SalaPrueba', fecha=datetime.now())
        sala.save()
        mensaje1 = Mensaje.objects.create(creador='Nicolas', contenido='Hola', sala=sala, es_url=False)
        mensaje1.save()
        mensaje2 = Mensaje.objects.create(creador='Nicolas', contenido='png', sala=sala, es_url=True)
        mensaje2.save()
        mensaje3 = Mensaje.objects.create(creador='Nicolas', contenido='hey', sala=sala, es_url=True)
        mensaje3.save()

        x, y, z = pie_pagina()

        self.assertEqual(x, 1) #textuales
        self.assertEqual(y, 2) #imagenes
        self.assertEqual(z, 1) #salas activas