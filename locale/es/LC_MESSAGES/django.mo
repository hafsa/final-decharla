��    3      �  G   L      h     i     o  K        �     �     �  
   �     
  
        !     (     0  �   5  _   �  �   Z  r   >     �     �     �     �     �     �               1     8     J  	   S     ]     n     �     �     �     �     �     �  	   �     �  %   	  �   1	     �	     
     
     
  	   *
  	   4
  "   >
     a
     �
  !   �
  B  �
            K        c     r     �  
   �     �  
   �     �     �     �  �   �  _   �  �   �  r   �     I     R     d     l     u     �     �     �     �     �     �  	   �     �                1     C     P     j     |  	   �     �  %   �  �   �     �     �     �     �  	   �  	   �  "   �     �       !   4        	             
   $                        %   ,   +   (   *                          &   !   1                 .                       -       "          #   0   '   3              /             )                                   2                 Ayuda Añadir mensaje Bienvenido, introduzca una contraseña válida para autenticarse por favor. Cerrar sesión Configuración Configuración actual Contenido: Contraseña Crear Sala Grande Guardar Hola Hola, si estás aquí es porque necesitas algo de ayuda. Espero que el siguiente resumen te ayude, pero si necesitas más asistencia, no dudes en contactarme vía email (hafsaeljauhari@gmail.com). Introduce un nombre para crear una sala o bien para entrar a una ya existente si así lo desea. La aplicación DeCharla consiste en un chat en el que cualquier persona puede escribir mensajes. El chat se estructura por salas que pueden tratar diversos temas, hobbies, aspiraciones... o simplemente ser de libre mensajería. La aplicación fue creada por la estudiante Hafsa El Jauhari Al Jaouhari durante el periodo de Mayo/Junio de 2023. Me gusta Me gustas totales Mediano Mensajes Mensajes no leídos Mensajes totales No hay mensajes añadidos No hay salas creadas Nombre Nombre de la sala Pequeño Principal PÁGINA DE AYUDA PÁGINA DE CONFIGURACIÓN PÁGINA DE LOGIN PÁGINA PRINCIPAL PÁGINA SALA Página de Configuración SALAS DISPONIBLES Sala Dinámica Sala Json Salas activas Seleccionar si el mensaje es una url: Tal y como está configurada la aplicación,  pueden crear salas todos aquellos usuarios autenticados a través de una contraseña válida proporcionada por el administrador de la aplicación. Tamaño de fuente Tipo de fuente Validar Ya NO me gusta imágenes textuales ¿En qué consiste la aplicación? ¿Quién creó la aplicación? ¿Quién puede crear salas? ¿Quién puede escribir mensajes? Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Ayuda Añadir mensaje Bienvenido, introduzca una contraseña válida para autenticarse por favor. Cerrar Sesión Configuración Configuración actual Contenido: Contraseña Crear Sala Grande Guardar Hola Hola, si estás aquí es porque necesitas algo de ayuda. Espero que el siguiente resumen te ayude, pero si necesitas más asistencia, no dudes en contactarme vía email (hafsaeljauhari@gmail.com). Introduce un nombre para crear una sala o bien para entrar a una ya existente si así lo desea. La aplicación DeCharla consiste en un chat en el que cualquier persona puede escribir mensajes. El chat se estructura por salas que pueden tratar diversos temas, hobbies, aspiraciones... o simplemente ser de libre mensajería. La aplicación fue creada por la estudiante Hafsa El Jauhari Al Jaouhari durante el periodo de Mayo/Junio de 2023. Me gusta Me gustas totales Mediano Mensajes Mensajes no leídos Mensajes totales No hay mensajes añadidos No hay salas creadas Nombre Nombre de la sala Pequeño Principal PÁGINA DE AYUDA PÁGINA DE CONFIGURACIÓN PÁGINA DE LOGIN PÁGINA PRINCIPAL PÁGINA SALA Página de Configuración SALAS DISPONIBLES Sala Dinámica Sala Json Salas activas Seleccionar si el mensaje es una url: Tal y como está configurada la aplicación,  pueden crear salas todos aquellos usuarios autenticados a través de una contraseña válida proporcionada por el administrador de la aplicación. Tamaño de fuente Tipo de fuente Validar Ya NO me gusta imágenes textuales ¿En qué consiste la aplicación? ¿Quién creó la aplicación? ¿Quién puede crear salas? ¿Quién puede escribir mensajes? 