# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Hafsa El Jauhari Al Jaouhari
* Titulación: Doble grado en ingeniería en sistemas de telecomunicación y ADE
* Cuenta en laboratorios: hafsa
* Cuenta URJC:  h.eljauhari.2020@alumnos.urjc.es
* Video básico (url): https://youtu.be/mPkmFWNlUL4 
* Video parte opcional (url): https://youtu.be/E6ttlzIRNjM 
* Despliegue (url): http://hafsa.pythonanywhere.com/
* Contraseñas: Holaquepasa
* Cuenta Admin Site: hafsa/Holaquepasa


## Resumen parte obligatoria

* Práctica construida en un proyecto Django/Python3.
* Almacenamiento de datos en SQLite3.
* Funcionalidad de cuentas de Admin Site para las tablas de las base de datos. 
* Utilización de plantillas jerárquicas para la realización del estilo de 
las páginas de la aplicación.
  (Creación de un archivo "base.html" que define los aspectos principales de todas las páginas.)
* Se sirven todas las páginas mencionadas en el enunciado de la práctica:  
--> Página Principal (localhost:8000/DeCharla/)  
--> Página de Ayuda (localhost:8000/DeCharla/help/)  
--> Página de cada Sala (localhost:8000/DeCharla/{sala}/)  
--> Página de Configuración (localhost:8000/DeCharla/configuracion/)  
--> Página de Admin (localhost:8000/Admin/)  
--> Página dinámica de cada sala (localhost:8000/DeCharla/sala_dinamica/{sala>})  
--> Página json de cada sala (localhost:8000/DeCharla/sala_json/{sala})  
--> Página de Login (localhost:8000/DeCharla/login/)
* Se proporcionan todos los elementos generales que aparecen en el enunciado para las páginas HTML.
  (Pueden consultarse en el archivo "base.html").  
* El marcado HTML se cumple según las condiciones del enunciado: se usan elementos "div" y elementos "header", "footer" para el
encabezado y pie de página. 
* Se usan hojas de estilo para el estilo de las páginas: "style.css" para el estilo interno de la aplicación y 
"stylelogin.css" para el estilo de la página de Login. 
* Para la maquetación se ha usado (como menciona el enunciado) bootstrap.
* Se ofrece el recurso JSON para cada sala, el cual devuelve los mensajes de la sala en el formato mencionado.
* Se ofrece la posibilidad de añadir mensajes vía solicitudes PUT en cuyo cuerpo se incluirán dichos mensajes en formato XML.
  (Si ya habías entrado en login desde ese navegador, es decir, tienes la cookie de sesión, no necesitarás de la cabecera
Autentication. En caso contrario, es imprescindible para poder autenticarse). #Añadir cabecera header en RESTClient. Se incluye mensajes.xml para facilitar la comprobación de este punto.
* En cuanto a la auténticación, se ha diseñado la aplicación para que solo puedan acceder a esta misma aquellos que 
hayan rellenado en esa misma sesión anteriormente el formulario de Login con alguna de contraseña válida. En caso contrario 
se redireccionará a este recurso. 
* Se ha tenido en cuenta para el diseño que en el menu no aparezca como opción la página actual.
* La aplicación se adapta perfectamente a las condiciones de tamaño tanto del telefono móvil como de la de un ordenador/portátil.  
* Se incluye una imagen como banner servida por la propia aplicación.  
* Se incluyen test Extremo a Extremo para cada uno de los recursos servidos por la aplicación.

## Lista partes opcionales

* Inclusión de un favicon del sitio. Se añade como icono de la aplicación.
* Permitir que las sesiones autenticadas se puedan terminar: se ofrece el botón de "cerrar sesión", tras el cual se elimina la cookie de sesión del navegador. En caso de cerrar la pestaña sin cerrar la sesión, en futuras visitas se continuará desde esa misma sesión y con los últimos datos guardados (configuraciones,nombre de charlador,mensajes no leídos...)
* Permitir votar las salas: se permite votar una vez cada sala (con la posibilidad de quitar el voto si ya no te gusta).
* Atención al idioma indicado por el visitante en el formulario de la página de login: se ofrece la posibilidad de ver las páginas de la aplicación tanto en inglés como en español. 
* Mejora de los tests de la práctica. Los test extremo a extremo de la aplicación no solo comprueban la funcionalidad básica 
en cuanto a HTTP sino que en muchos de ellos se profundiza. Además se han añadido test unitarios y también se ha añadido 
el archivo gitlab-ci-yml para su correspondiente automatización dentro de GITLAB.
